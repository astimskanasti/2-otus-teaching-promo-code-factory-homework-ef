﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }
        
        public Customer ToCustomer(IEnumerable<Preference> preferences= null, Customer customer = null)
        {
            if(customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            
            customer.FirstName = FirstName;
            customer.LastName = LastName;
            customer.Email = Email;

            if (preferences != null)
            {
                customer.CustomerPreferences = preferences.Select(x => new CustomerPreference()
                {
                    Customer = customer,
                    PreferenceId = x.Id,
                }).ToList();
            }

            return customer;
        }
    }
}