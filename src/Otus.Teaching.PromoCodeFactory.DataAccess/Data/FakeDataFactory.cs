﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                //Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };
        
        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("ef8f299f-92d7-459f-896e-078ed53ef99c"),
                Code = "12345",
                ServiceInfo = "Кафе",
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(20),
                PartnerName= "Мы варим кофе",
                PreferenceId = Preferences.FirstOrDefault(x => x.Name == "Театр").Id,
            },
            new PromoCode()
            {
                Id = Guid.Parse("c5bda62e-fc74-4256-a956-4760b3858cbd"),
                Code = "23456",
                ServiceInfo = "Прокат авто",
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(10),
                PartnerName= "Дрын дрын",
                PreferenceId = Preferences.FirstOrDefault(x => x.Name == "Семья").Id,
            },
            new PromoCode()
            {
                Id = Guid.Parse("76424c47-68d2-472d-abb8-33cfa8cc0c84"),
                Code = "34567",
                ServiceInfo = "Детские товары",
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(2),
                PartnerName= "Дочки-сыночки",
                PreferenceId = Preferences.FirstOrDefault(x => x.Name == "Семья").Id,
            }
        };

        public static IEnumerable<Customer> Customers
        {
            get
            {
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        PromoCodeId = PromoCodes.FirstOrDefault(x => x.PartnerName == "Мы варим кофе").Id,
                    },
                    new Customer()
                    {
                        Id = Guid.Parse("96424c47-68d2-472d-abb8-33cfa8cc0c84"),
                        Email = "PETROV@mail.ru",
                        FirstName = "Климов",
                        LastName = "Андрей",
                        PromoCodeId = PromoCodes.FirstOrDefault(x => x.PartnerName == "Дрын дрын").Id,
                    }
                };

                return customers;
            }
        }
    }
}