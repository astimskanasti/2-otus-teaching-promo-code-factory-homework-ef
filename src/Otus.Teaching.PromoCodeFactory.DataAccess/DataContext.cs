using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
namespace Otus.Teaching.PromoCodeFactory.DataAccess;


public partial class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    
    {
    }
    
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Role> Roles { get; set; }
    public virtual DbSet<Preference> Preferences { get; set; }
    public virtual DbSet<Preference> CustomerPreference { get; set; }
    public virtual DbSet<Customer> Customers { get; set; }
    public virtual DbSet<PromoCode> PromoCodes { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        base.OnModelCreating(modelBuilder);
    }
}