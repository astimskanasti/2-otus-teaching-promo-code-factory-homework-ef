using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<TEntity>
    : IRepository<TEntity> 
    where TEntity : BaseEntity
{
    private DataContext DbContext { get; }

    public EfRepository(DataContext dbContext)
    {
        DbContext = dbContext;
    }

    public Task<TEntity> GetByIdAsync(Guid id)
        => DbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);

    public async Task<Guid> AddAsync(TEntity item)
    {
        await DbContext.AddAsync(item);
        await DbContext.SaveChangesAsync();

        return item.Id;
    }

    public async Task DeleteAsync(TEntity item)
    {
        DbContext.Set<TEntity>().Remove(item);
        await DbContext.SaveChangesAsync();
    }

    public Task<IEnumerable<TEntity>> GetAllAsync()
        => Task.FromResult(DbContext.Set<TEntity>().AsEnumerable());


    public Task UpdateAsync(TEntity item)
        => DbContext.SaveChangesAsync();
}

