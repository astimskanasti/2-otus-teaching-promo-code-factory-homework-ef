using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public abstract class EntityConfigurationBase<TEntity> : IEntityTypeConfiguration<TEntity>
    where TEntity : BaseEntity
{
    public void Configure(EntityTypeBuilder<TEntity> builder)
    {
        builder.HasKey(p => p.Id);
    }
    protected virtual void ConfigureProperties(EntityTypeBuilder<TEntity> builder)
    {
    }

    protected virtual void ConfigureNavigationProperties(EntityTypeBuilder<TEntity> builder)
    {
    }

    protected virtual void ConfigureIndexes(EntityTypeBuilder<TEntity> builder)
    {
    }
    
}