using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class CustomerConfiguration: EntityConfigurationBase<Customer>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<Customer> builder)
    {
        builder
            .HasMany(e => e.CustomerPreferences)
            .WithMany();
        
        builder
            .HasOne(e => e.PromoCode)                
            .WithMany()
            .HasForeignKey(e => e.PromoCodeId);
    }

    protected override void ConfigureProperties(EntityTypeBuilder<Customer> builder)
    {
        builder.Property(customer => customer.FirstName).HasMaxLength(50);
        builder.Property(customer => customer.LastName).HasMaxLength(50);
        builder.Property(customer => customer.Email).HasMaxLength(50);
    }
}