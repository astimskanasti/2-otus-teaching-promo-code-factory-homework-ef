using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class CustomerPreferenceConfiguration: EntityConfigurationBase<CustomerPreference>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<CustomerPreference> builder)
    {
        builder
            .HasOne(bc => bc.Customer)
            .WithMany()
            .HasForeignKey(bc => bc.CustomerId);  
        builder
            .HasOne(bc => bc.Preference)
            .WithMany()
            .HasForeignKey(bc => bc.PreferenceId); 
    }
}