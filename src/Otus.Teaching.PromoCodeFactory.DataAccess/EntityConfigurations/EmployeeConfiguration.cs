using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class EmployeeConfiguration : EntityConfigurationBase<Employee>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<Employee> builder)
    {
        builder
            .HasOne(e => e.Role)
            .WithMany()
            .HasForeignKey(e => e.RoleId);
    }

    protected override void ConfigureProperties(EntityTypeBuilder<Employee> builder)
    {
        builder.Property(employee => employee.FirstName).HasMaxLength(50);
        builder.Property(employee => employee.LastName).HasMaxLength(50);
        builder.Property(employee => employee.Email).HasMaxLength(50);
    }
}