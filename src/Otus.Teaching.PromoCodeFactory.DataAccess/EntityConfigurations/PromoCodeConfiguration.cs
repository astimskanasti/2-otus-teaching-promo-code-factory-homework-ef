using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityConfigurations;

public class PromoCodeConfiguration: EntityConfigurationBase<PromoCode>
{
    protected override void ConfigureNavigationProperties(EntityTypeBuilder<PromoCode> builder)
    {
        builder
            .HasOne(e => e.Preference)                
            .WithMany()
            .HasForeignKey(e => e.PreferenceId);
    }

    protected override void ConfigureProperties(EntityTypeBuilder<PromoCode> builder)
    {
        builder.Property(promoCode => promoCode.Code).HasMaxLength(50);
        builder.Property(promoCode => promoCode.ServiceInfo).HasMaxLength(50);
        builder.Property(promoCode => promoCode.PartnerName).HasMaxLength(50);
    }
}